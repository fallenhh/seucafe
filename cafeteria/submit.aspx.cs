﻿//Copyright (c) 2018, fallenhh and Zambashilidye 
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the SEU nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.



using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace cafeteria
{
    public partial class submit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["rss"] == null)
                Response.Redirect("order.aspx");
            label1.Text = Session["rss"].ToString();
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string dat = Session["dat"].ToString();
            warning.Text = "";
            if (File.Exists("d://cafeteria//lock"))
            {
                warning.Text = "Server busy, Try again later";
                return;
            }
            //File.Create("d://cafeteria//lock");
            FileStream lck = new FileStream("d://cafeteria//lock", FileMode.CreateNew);
            lck.Close();
            FileStream fs = new FileStream("d://cafeteria//text.txt", FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            //开始写入
            int suc = 0;
            if (dat != "")
            {
                sw.Write(dat + "\n");
                suc = 1;
            }
            //清空缓冲区
            sw.Flush();
            //关闭流
            sw.Close();
            fs.Close();
            File.Delete("d://cafeteria//lock");
            
            Session["s"] = suc;
            Response.Redirect("success.aspx");
        }
        protected void Button2_Click(object sender, EventArgs e) {
            Response.Redirect("order.aspx");
        }
    }
}
