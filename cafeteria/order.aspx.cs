﻿//Copyright (c) 2018, fallenhh and Zambashilidye 
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the SEU nor the
//      names of its contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.



using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace cafeteria
{
    public partial class order : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            int time=DateTime.Now.Hour;


            int[] cost = new int[4] { 57, 87, 79, 68 };
            if (Name.Text == "")
            {
                warning.Text = "Please type in your name!";
                return;
            };
            if (!Regex.IsMatch(RoomNo.Text, @"\d\d\d"))
            {
                warning.Text = "Please check the room number!";
                return;
            };
            int b = Convert.ToInt32(RoomNo.Text);
            if (b < 101 || b > 640 || b % 100 > 40)
            {
                warning.Text = "Please check the room number!";
                return;
            };
            if (select1.SelectedItem.Value == "0" && select2.SelectedItem.Value == "0" && select3.SelectedItem.Value == "0" && select4.SelectedItem.Value == "0")
            {
                warning.Text = "Please order SOMETHING!";
                return;
            }
            if (TimeSelect.Text == "0" && time > 12 ) {
                warning.Text = "Too late to order lunch!";
                return;
            }
            if (TimeSelect.Text == "1" && time > 18)
            {
                warning.Text = "Too late to order dinner!";
                return;
            }
            string label1;
            string data="";
            label1 = "Name:"+Name.Text + " Dorm:"+DormName.SelectedItem.Text + DormBuilding.SelectedItem.Text + b.ToString()+"<br>"+"Time:" + TimeSelect.SelectedItem.Text+ "<br>" + "Your Order:"+ "<br>";
            data=data+ Name.Text + " "+DormName.SelectedItem.Value+" "+DormBuilding.SelectedItem.Value+" "+b+" "+TimeSelect.SelectedItem.Value+" ";
            if (select1.SelectedItem.Value != "0") { label1 += "Hamburger" + " "+select1.SelectedItem.Text+ "<br>"; }
            data += select1.SelectedValue+" ";
            if (select2.SelectedItem.Value != "0") { label1 += "Cheeseburger" + " " + select2.SelectedItem.Text + "<br>"; }
            data += select2.SelectedValue + " ";
            if (select3.SelectedItem.Value != "0") { label1 += "Beefburger" + " " + select3.SelectedItem.Text + "<br>"; }
            data += select3.SelectedValue + " ";
            if (select4.SelectedItem.Value != "0") { label1 += "Chickenburger" + " " + select4.SelectedItem.Text + "<br>"; }
            data += select4.SelectedValue + " ";
            int discount = 0;
            if (Code.Text == "cheeseispower")
            {
                codejudge.Text = "Bingo!";
                discount = 20;
                data += "1 ";
            }
            else if (Code.Text != "") { codejudge.Text = "Wrong!"; data += "0 "; return; }

            string co = (-discount + Convert.ToInt32(select1.SelectedItem.Value) * cost[0] + Convert.ToInt32(select2.SelectedItem.Value) * cost[1] + Convert.ToInt32(select3.SelectedItem.Value) * cost[2] + Convert.ToInt32(select4.SelectedItem.Value) * cost[3]).ToString();
            label1 += "Discount:" + discount.ToString() + "<br>"+"Total:" +co+"<br>";
            data += (co+" "+"\n");
            Session["rss"]=label1;
            Session["dat"] = data;
            Response.Redirect("submit.aspx");
        }
    }
}
