﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="submit.aspx.cs" Inherits="cafeteria.submit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @import "Global.css";
</style>
    <title>Submit</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 200px ; background-color: wheat; opacity:0.9; stop-opacity:0.95">
     <p>
            <asp:Label id="label1" runat="server"></asp:Label>
        </p>
        <asp:Button id="Button_Submit" runat="server" Text="Submit" OnClick="Button1_Click" CssClass="Button" Width="134px"></asp:Button>
        <asp:Button id="Button_Back" runat="server" Text="Back" OnClick="Button2_Click" CssClass="Button" Width="145px"></asp:Button>

        <p>
            <asp:Label id="warning" runat="server" CssClass="Warning"></asp:Label>
        </p>
    </div>
    </form>
</body>
</html>
