﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="order.aspx.cs" Inherits="cafeteria.order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    @import "Global.css";
</style>
    <title>Order</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 200px ; background-color: wheat; opacity:0.9; stop-opacity:0.95">
     <td>Name:</td>
    <asp:TextBox id="Name" runat="server" Height="16px" Width="279px"></asp:TextBox>
     <p>
            <asp:DropDownList id="DormName" runat="server">
                <asp:ListItem Value="Tao" Selected="True">T</asp:ListItem>
                <asp:ListItem Value="Mei">M</asp:ListItem>
                <asp:ListItem Value="Ju">J</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList id="DormBuilding" runat="server">
                <asp:ListItem Value="1" Selected="True">B1</asp:ListItem>
                <asp:ListItem Value="2">B2</asp:ListItem>
                <asp:ListItem Value="3">B3</asp:ListItem>
                <asp:ListItem Value="4">B4</asp:ListItem>
                <asp:ListItem Value="5">B5</asp:ListItem>
                <asp:ListItem Value="6">B6</asp:ListItem>
                <asp:ListItem Value="7">B7</asp:ListItem>
                <asp:ListItem Value="8">B8</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox id="RoomNo" runat="server" Width="220px"></asp:TextBox>
        </p>


            <asp:RadioButtonList id="TimeSelect" runat="server" Width="251px" Height="116px">
                <asp:ListItem Value="0">Lunch</asp:ListItem>
                <asp:ListItem Value="1">Dinner</asp:ListItem>
                <asp:ListItem Value="2">Tomorrow Breakfast</asp:ListItem>
            </asp:RadioButtonList>

        <table border="0" style="height: 120px; width: 417px">
            <tr>
                <td>Hamburger     57</td>
                <td><asp:DropDownList id="select1" runat="server" Width="89px">
                 <asp:ListItem Value="0" Selected="True">x0</asp:ListItem>
                <asp:ListItem Value="1">X1</asp:ListItem>
                <asp:ListItem Value="2">X2</asp:ListItem>
                <asp:ListItem Value="3">X3</asp:ListItem>
                <asp:ListItem Value="4">X4</asp:ListItem>

            </asp:DropDownList></td>
            </tr>       
            <tr>
                <td>Cheeseburger 87</td>
                <td><asp:DropDownList id="select2" runat="server" Width="89px">
                 <asp:ListItem Value="0" Selected="True">x0</asp:ListItem>
                <asp:ListItem Value="1">X1</asp:ListItem>
                <asp:ListItem Value="2">X2</asp:ListItem>
                <asp:ListItem Value="3">X3</asp:ListItem>
                <asp:ListItem Value="4">X4</asp:ListItem>

            </asp:DropDownList></td>
            </tr>       
            <tr>
                <td>Beefburger 79</td>
                <td><asp:DropDownList id="select3" runat="server" Width="89px">
                 <asp:ListItem Value="0" Selected="True">x0</asp:ListItem>
                <asp:ListItem Value="1">X1</asp:ListItem>
                <asp:ListItem Value="2">X2</asp:ListItem>
                <asp:ListItem Value="3">X3</asp:ListItem>
                <asp:ListItem Value="4">X4</asp:ListItem>

            </asp:DropDownList></td>
            </tr>       
            <tr>
                <td>Chickenburger 68</td>
                <td><asp:DropDownList id="select4" runat="server" Width="89px">
                 <asp:ListItem Value="0" Selected="True">x0</asp:ListItem>
                <asp:ListItem Value="1">X1</asp:ListItem>
                <asp:ListItem Value="2">X2</asp:ListItem>
                <asp:ListItem Value="3">X3</asp:ListItem>
                <asp:ListItem Value="4">X4</asp:ListItem>

            </asp:DropDownList></td>
              
                
                 CouponCode
    <asp:TextBox id="Code" runat="server"></asp:TextBox><asp:Label id="codejudge" runat="server" ForeColor="Red"></asp:Label>
                    
            </tr>       
            <tr>
                <td></td>
                <td><asp:Button id="Button1" runat="server" Text="CheckOut" OnClick="Button1_Click" Width="89px" CssClass="Button" Font-Size="Small"></asp:Button></td>
            </tr>       
        </table>
        <p>
            <asp:Label id="warning" runat="server" ForeColor="Red" CssClass=".Warning"></asp:Label>
        </p>
       
    </div>
    </form>
</body>
</html>
