﻿CREATE TABLE [dbo].[Order]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NCHAR(10) NULL, 
    [Location] NCHAR(10) NULL, 
    [Time] INT NULL, 
    [Order] NCHAR(10) NULL, 
    [Price] FLOAT NULL
)
