## SEU Cafeteria Online Order System
### The final project for the network programming
author: fallenhh, Zambashilidye

## Description
If you don't know what this project is for, just leave it alone.
To make the website work, you may need to create the folder __D://cafeteria__
or alternatively change the corresponding code in the __submit.aspx.cs__
The sub-project __Database1__ is in TODO List, please just ignore it.

All the resources used for the project are just for the presentational use.

## Dependcies
- Microsoft Visual Studio >= 2015
- .NET >= 4.0
- C#
- IIS and ASP.NET component (Optional) 

## LICENSE
[BSD License](LICENSE)

